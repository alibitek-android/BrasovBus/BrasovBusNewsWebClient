package com.kroganinventiv.dto;

import com.kroganinventiv.model.User;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 6/5/12
 * Time: 12:59 AM
 * To change this template use File | Settings | File Templates.
 */
public class UserListDto {
    private List<User> users;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
