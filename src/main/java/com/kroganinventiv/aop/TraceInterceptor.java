package com.kroganinventiv.aop;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 6/5/12
 * Time: 12:58 AM
 * To change this template use File | Settings | File Templates.
 */

import net.spy.memcached.compat.log.Level;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.logging.Log;
import org.springframework.aop.interceptor.CustomizableTraceInterceptor;

import java.util.logging.Logger;

/**
 * Extends {@link org.springframework.aop.interceptor.CustomizableTraceInterceptor} to provide custom logging levels
 */
public class TraceInterceptor extends CustomizableTraceInterceptor {

    private static final long serialVersionUID = 287162721460370957L;
    protected static Logger logger4J = Logger.getLogger("aop");

    @Override
    protected void writeToLog(Log logger, String message, Throwable ex) {
        if (ex != null) {
            logger4J.log(java.util.logging.Level.ALL, message, ex);
        } else {
            logger4J.log(java.util.logging.Level.ALL, message);
        }
    }

    @Override
    protected boolean isInterceptorEnabled(MethodInvocation invocation, Log logger) {
        return true;
    }
}
