package com.kroganinventiv.model;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Alex Butum
 * Date: 3/21/12
 * Time: 4:39 PM
 */

public class News {
    private int id;
    private String title;
    private Date date;
    private String subtitle;
    private String contents;

    public News() {
    }

    public News(int id, String title, Date date, String subtitle, String contents) {
        this.id = id;
        this.title = title;
        this.date = date;
        this.subtitle = subtitle;
        this.contents = contents;
    }

    public String getTitle() {
        return title;
    }

    public Date getDate() {
        return date;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public String getContents() {
        return contents;
    }

    public int getId() {
        return id;
    }
}
