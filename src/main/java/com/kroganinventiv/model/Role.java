package com.kroganinventiv.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 6/5/12
 * Time: 12:58 AM
 * To change this template use File | Settings | File Templates.
 */
@Document
public class Role {

    @Id
    private String id;

    private Integer role;

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
