package com.kroganinventiv.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 6/5/12
 * Time: 12:58 AM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/")
public class MediatorController {

    @RequestMapping
    public String index() {
        return "redirect:/users";
    }
}
