package com.kroganinventiv.repository;

import com.kroganinventiv.model.Role;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 6/5/12
 * Time: 12:59 AM
 * To change this template use File | Settings | File Templates.
 */
public interface RoleRepository extends MongoRepository<Role, String> {
}
